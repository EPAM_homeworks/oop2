class Metaenum(type):
    
    def __new__(metaclass, name, parents, classdict):
        
        list_of_tuples = []
        
        for key, value in classdict.items():
            if not key.endswith('__'):
                list_of_tuples.append((key,value))
                
        classdict['tuples'] = list_of_tuples
        enum = super().__new__(metaclass, name, parents, classdict)
        enumeration = []
        
        for key, value in list_of_tuples:
            member = enum(value)
            enumeration.append(member)
            setattr(enum, key, member)
        enum._enumeration = tuple(enumeration)
        return enum
     
        
    def __iter__(cls):
        return iter(cls._enumeration)
        
    def __repr__(self):
        return f'{__class__.__name__}.{self.name}'
    
    def __getitem__(cls, key):
        for k, v in cls.tuples:
            if key == k:
                return v
        raise KeyError(key)
    
class Enum(metaclass=Metaenum):

    def __new__(cls, value):
        if value not in map(lambda x: x[1], cls.tuples):
            raise KeyError(f'{value} is not a valid {cls.__name__}')
        for member in cls._enumeration:
            if member.value == value:
                return member
        member = super().__new__(cls)
        for key, v in cls.tuples:
            if value == v:
                member.name = key
        member.__class__ = cls
        member.value = value
        return member

        
    def __str__(self):
        return '{}.{}'.format(type(self).__name__, self.name)
    
    
    def __repr__(self):
        return '<{},(): ()'.format(type(self).__name__, self.name, self.value)
        

class Direction(Enum):
    north = 0
    east = 90
    south = 180
    west = 270

print(Direction.north)  # <Direction.north: 0>
print(Direction.south)  # <Direction.south: 180>
print(Direction.north.name)  # north
print(Direction.north.value)  # 0

for d in Direction:
    print(d)
    
print(id(Direction.north))  
print(id(Direction(0))) 

Direction(30)

print(Direction['west']) 
Direction['north-west']

